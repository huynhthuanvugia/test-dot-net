﻿using System.Net;

namespace Inter.ExcepptionHandllers
{
    public class BadRequestException : CustomException
    {
        public BadRequestException(string message)
            : base(message, null, HttpStatusCode.NotFound)
        {
        }
    }
}
