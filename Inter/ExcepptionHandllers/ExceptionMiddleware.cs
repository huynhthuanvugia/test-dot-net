﻿namespace Inter.ExcepptionHandllers
{
    public class ExceptionMiddleware : IMiddleware
    {
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(ILogger<ExceptionMiddleware> logger)
        {
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync("ERROR: " + ex);
                _logger.LogError("ERROR: " + ex.Message);
                await HandleException(context, ex);
                //context.Response.WriteAsync(ex.Message);

            }
        }

        private static Task HandleException(HttpContext context, Exception ex)
        {
            int statusCode = StatusCodes.Status400BadRequest;
            switch (ex)
            {
                case NotFoundException _:
                    statusCode = StatusCodes.Status404NotFound;
                    break;
                case BadRequestException _:
                    statusCode = StatusCodes.Status400BadRequest;
                    break;
                    //case DivideByZeroException _:
                    //    statusCode = StatusCodes.Status400BadRequest;
                    //    break;
                    // you can define some more exceptions, according to need
            }
            var errorResponse = new ErrorDetails
            {
                StatusCode = statusCode,
                Message = ex.Message
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(errorResponse.ToString());
        }
    }
}
