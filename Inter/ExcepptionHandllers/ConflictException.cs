﻿using System.Net;

namespace Inter.ExcepptionHandllers
{
    public class ConflictException : CustomException
    {
        public ConflictException(string message)
            : base(message, null, HttpStatusCode.Conflict) { }
    }
}
