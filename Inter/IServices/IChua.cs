﻿using Inter.ResponseDTO;

namespace Inter.IServices
{
    public interface IChua
    {
        List<ChuaResponseToFilterDTO> GetAllToFilter();
    }
}
