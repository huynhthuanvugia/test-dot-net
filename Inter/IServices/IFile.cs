﻿namespace Inter.IServices
{
    public interface IFile
    {
        FileStream DownloadFile(string fileName);
        string UploadFile(IFormFile formFile);
    }
}
