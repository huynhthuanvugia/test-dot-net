﻿using Inter.Entities;

namespace Inter.ResponseDTO
{
    public class PhatTuResponseDTO
    {
        public int Id { get; set; }
        public string Ho { get; set; }
        public string TenDem { get; set; }
        public string Ten { get; set; }
        public string PhapDanh { get; set; }
        public string AnhChup { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public string NgaySinh { get; set; }
        public string NgayXuatGia { get; set; }
        public bool DaHoanTuc { get; set; }
        public string NgayHoanTuc { get; set; }
        public string GioiTinh { get; set; }
        public string KieuThanhVien { get; set; }
        public string NgayCapNhat { get; set; }
        public string TenChua { get; set; }

        //

        public static PhatTuResponseDTO Mapping(PhatTu phatTu)
        {
            return new PhatTuResponseDTO
            {
                Id = phatTu.Id,
                Ho = phatTu.Ho,
                TenDem = phatTu.TenDem,
                Ten = phatTu.Ten,
                PhapDanh = phatTu.PhapDanh,
                AnhChup = phatTu.AnhChup,
                SoDienThoai = phatTu.SoDienThoai,
                Email = phatTu.Email,
                NgaySinh = phatTu.NgaySinh.ToShortDateString(),
                NgayXuatGia = phatTu.NgayXuatGia.ToShortDateString(),
                DaHoanTuc = phatTu.DaHoanTuc,
                NgayHoanTuc = phatTu.NgayHoanTuc.ToShortDateString(),
                GioiTinh = phatTu.GioiTinh == 1 ? "Nam" : "Nữ",
                KieuThanhVien = phatTu.KieuThanhVien.TenKieu,
                NgayCapNhat = phatTu.NgayCapNhat.ToShortDateString(),
                TenChua = phatTu.Chua.TenChua
            };
        }
    }
}
