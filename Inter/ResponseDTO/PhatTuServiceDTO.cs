﻿namespace Inter.ResponseDTO
{
    public class PhatTuServiceDTO
    {
        public List<PhatTuResponseDTO> PhatTuList { get; set; }
        public double TotalPage { get; set; }
        public int Page { get; set; }
        public int TotalItem { get; set; }
    }
}
