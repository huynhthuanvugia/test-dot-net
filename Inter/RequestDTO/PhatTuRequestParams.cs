﻿using Inter.Common;
using Inter.Entities;
using Microsoft.IdentityModel.Tokens;

namespace Inter.RequestDTO
{
    public class PhatTuRequestParams
    {
        public string? Search { get; set; } = "";
        public int Page { get; set; } = 1;
        public int Size { get; set; } = 5;
        public string? ChuaFilter { get; set; }
        public string? FilterBy { get; set; } = "ChuaId";
        public string? SortBy { get; set; } = "NgayCapNhat";
        public string? SortOrder { get; set; } = "desc";

        public bool Filter(PhatTu phatTu)
        {
            return
                (this.ChuaFilter.IsNullOrEmpty() ? true : this.ChuaFilter.Split(',').Select(i => int.Parse(i)).ToList().Contains(phatTu.ChuaId))
                && (this.Search.IsNullOrEmpty() ? true : phatTu.PhapDanhUnsigned.Contains(MyUtils.LocDau(this.Search)))
                ;
        }

        public object Sort(PhatTu phatTu)
        {
            return phatTu.GetType().GetProperty(this.SortBy ?? "NgayCapNhat").GetValue(phatTu, null);
        }

        public int Skip()
        {
            return (this.Page - 1) * this.Size;
        }
    }
}
