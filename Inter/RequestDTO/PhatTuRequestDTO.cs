﻿using Inter.Common;
using Inter.Entities;

namespace Inter.RequestDTO
{
    public class PhatTuRequestDTO
    {
        public int? Id { get; set; }
        public string Ho { get; set; }
        public string TenDem { get; set; }
        public string Ten { get; set; }
        public string PhapDanh { get; set; }
        public IFormFile? File { get; set; }
        public string? AnhChup { get; set; }
        public string SoDienThoai { get; set; }
        public string Email { get; set; }
        public DateTime NgaySinh { get; set; }
        public DateTime NgayXuatGia { get; set; }
        public bool DaHoanTuc { get; set; }
        public DateTime NgayHoanTuc { get; set; }
        public int GioiTinh { get; set; }
        public int KieuThanhVienId { get; set; }
        public KieuThanhVien? KieuThanhVien { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public int ChuaId { get; set; }
        public Chua? Chua { get; set; }

        //
        public PhatTu Mapping()
        {
            return new PhatTu()
            {
                Id = this.Id ?? 0,
                Ho = this.Ho,
                TenDem = this.TenDem,
                Ten = this.Ten,
                PhapDanh = this.PhapDanh,
                AnhChup = this.AnhChup,
                SoDienThoai = this.SoDienThoai,
                Email = this.Email,
                NgaySinh = this.NgaySinh,
                NgayXuatGia = this.NgayXuatGia,
                DaHoanTuc = this.DaHoanTuc,
                NgayHoanTuc = this.NgayHoanTuc,
                GioiTinh = this.GioiTinh,
                KieuThanhVienId = this.KieuThanhVienId,
                NgayCapNhat = DateTime.Now,
                ChuaId = this.ChuaId,
                PhapDanhUnsigned = MyUtils.LocDau(this.PhapDanh)
            };
        }

    }
}
