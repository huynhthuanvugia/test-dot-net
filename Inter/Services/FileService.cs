﻿using Inter.IServices;

namespace Inter.Services
{
    public class FileService : IFile
    {
        private readonly string URL_BASE = "https://localhost:7123/file/";

        public string UploadFile(IFormFile formFile)
        {
            if (formFile == null) return null;
            var fileName = Path.GetFileName(formFile.FileName);
            var contentType = formFile.ContentType;

            var uniqueFileName = GetUniqueFileName(fileName);
            var filePath = Path.Combine("uploads", uniqueFileName);
            FileStream fileStream = new FileStream(filePath, FileMode.Create);

            formFile.CopyTo(fileStream);
            fileStream.Close();

            return URL_BASE + uniqueFileName;
        }


        public FileStream DownloadFile(string fileName)
        {
            string link = "uploads/" + fileName;
            FileStream fileStream = new FileStream(link, FileMode.Open);
            return fileStream;
        }


        //
        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                + "_"
                + Guid.NewGuid().ToString().Substring(0, 5)
                + Path.GetExtension(fileName)
                ;
        }
    }
}
