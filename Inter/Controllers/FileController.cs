﻿using Inter.IServices;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Inter.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private IFile fileService;

        public FileController(IFile _fileService)
        {
            fileService = _fileService;
        }

        // GET: <FileController>
        [HttpGet("{fileName}")]
        public IActionResult Get(string fileName)
        {
            var contentType = "image/png";
            FileStream fileStream = fileService.DownloadFile(fileName);
            return File(fileStream, contentType);
        }


    }
}
