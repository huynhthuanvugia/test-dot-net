﻿using Inter.IServices;
using Inter.RequestDTO;
using Inter.ResponseDTO;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Inter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhatTuController : ControllerBase
    {
        private readonly IPhatTu phatTuService;
        private readonly IFile fileService;

        public PhatTuController(IPhatTu _phatTuService, IFile _fileService)
        {
            this.phatTuService = _phatTuService;
            this.fileService = _fileService;
        }


        // GET: api/<PhatTuController>

        [HttpGet]
        public IActionResult GetAll([FromQuery] PhatTuRequestParams phatTuParams)
        {
            PhatTuServiceDTO phatTuDTO = phatTuService.GetAll(phatTuParams);
            Response.Headers.Add("Total-Page", phatTuDTO.TotalPage.ToString());
            Response.Headers.Add("Total-Item", phatTuDTO.TotalItem.ToString());
            Response.Headers.Add("Page", phatTuParams.Page.ToString());
            Response.Headers.Add("Items-Per-Page", phatTuParams.Size.ToString());

            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE");
            Response.Headers.Add("Access-Control-Expose-Headers", "Authorization, x-xsrf-token, " +
                "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, " +
                "Access-Control-Request-Method, Access-Control-Request-Headers, Page, Total-Item, Total-Page");
            return Ok(phatTuDTO.PhatTuList);
        }

        // GET api/<PhatTuController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<PhatTuController>
        [HttpPost]
        public IActionResult Post([FromForm] PhatTuRequestDTO phatTuDTO)
        {
            string pathFile = fileService.UploadFile(phatTuDTO.File);
            phatTuDTO.AnhChup = pathFile;

            return Ok(phatTuService.Create(phatTuDTO.Mapping()));
        }

        // PUT api/<PhatTuController>
        [HttpPut]
        public IActionResult Put([FromForm] PhatTuRequestDTO phatTuDTO)
        {
            //Console.WriteLine("fileService" + fileService.GetHashCode());
            //Console.WriteLine("phatTuService" + phatTuService.GetHashCode());
            string pathFile = fileService.UploadFile(phatTuDTO.File);
            phatTuDTO.AnhChup = pathFile ?? phatTuDTO.AnhChup;

            return Ok(phatTuService.Update(phatTuDTO.Mapping()));
        }

        // DELETE api/<PhatTuController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return Ok(phatTuService.Delete(id));
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }
    }
}
