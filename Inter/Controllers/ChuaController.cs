﻿using Inter.IServices;
using Inter.ResponseDTO;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Inter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChuaController : ControllerBase
    {
        private readonly IChua chuaService;

        public ChuaController(IChua _chuaService)
        {
            this.chuaService = _chuaService;
        }



        // GET: api/<ChuaController>
        [HttpGet("filter")]
        public List<ChuaResponseToFilterDTO> GetAllToFilter()
        {
            return chuaService.GetAllToFilter();
        }

    }
}
