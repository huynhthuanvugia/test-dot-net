﻿namespace Inter.Entities
{
    public class DonDangKy
    {
        public int Id { get; set; }
        public int PhatTuId { get; set; }
        public int TrangThaiDon { get; set; }
        public DateTime NgayGuiDon { get; set; }
        public DateTime NgayXuLy { get; set; }
        public int NguoiXuLyId { get; set; }

    }
}
