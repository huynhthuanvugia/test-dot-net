﻿namespace Inter.Entities
{
    public class PhatTuDaoTrang
    {
        public int Id { get; set; }
        public int? PhatTuId { get; set; }
        public int DaoTrangId { get; set; }
        public bool DaThamGia { get; set; }
        public string LyDoKhongThamGia { get; set; }

    }
}
