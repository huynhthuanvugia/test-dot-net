﻿namespace Inter.Entities
{
    public class Chua
    {
        public int Id { get; set; }
        public string TenChua { get; set; }
        public DateTime NgayThanhLap { get; set; }
        public string DiaChi { get; set; }
        public string TruTri { get; set; }
        public DateTime CapNhat { get; set; }

    }
}
